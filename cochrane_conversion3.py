#splitting the text files to create the jsons

import json
import glob
import os.path
import sys
import re



def getTextFiles(path):
    print('textfunction',path)
    return glob.glob('{}/*.txt'.format(path))

def txtToJson(txt_file):
    data = {}
    op_file_name = os.path.dirname(txt_file).rsplit('/',1)[0]+ '/json/' + os.path.basename(txt_file).split('.')[0] + '.json'

    with open(txt_file, 'r', encoding='utf-8',errors='ignore') as file:
        for line in file:

            if len(line) > 2:
                try:
                    line = line.split('\n')[0]
                    key_value = line.split(': ', 1)
                    key, value = key_value[0], key_value[1]
                    if key in data:
                        if isinstance(data[key], str):
                            val_list = [data[key]]
                            val_list.append(value)
                            data[key] = val_list #= json.loads(line)
                        else:
                            data[key].append(value)
                    else:
                        data[key] = value
                except:
                    print("Error in file -- ", op_file_name)


    with open(op_file_name, 'w', encoding='utf-8') as fp:
        json.dump(data, fp)


def splittxt(txt_file):
    data = []
    op_file_name = ''
    last_record = ''
    ID = ''
    with open(txt_file, 'r', encoding='utf-8',errors='ignore') as file:
        for line in file:
            if "Record" in line.split(' ')[0] and last_record != line:
                last_record = line
                if not op_file_name and len(data) > 0:
                    op_file = '{}{}'.format(str(ID), '.txt')
                    op_file_name = '{}/text/{}'.format(os.path.dirname(txt_file), op_file)
                    writetoFile(op_file_name, data)

                data = []
                op_file_name = None

            else:
                ##getting ID of record
                regex_ID = r"^ID: "
                if re.match(regex_ID, line):
                    ID = (line.split(": ")[1]).split('\n')[0]

                if not re.match('^\n$', line):
                    data.append(line)


        if not op_file_name and data:
            op_file = '{}{}'.format(str(ID), '.txt')
            op_file_name = '{}/text/{}'.format(os.path.dirname(txt_file), op_file)
            writetoFile(op_file_name, data)
            op_file_name, data = None, []

def writetoFile(op_file_name, data):
    with open(op_file_name, 'w', encoding='utf-8',errors='ignore') as of:
        for item in data:
            of.write(item)
        of.close()




def main():#(argv):
    path = 'C:/Users/Mayur Sonawane/Downloads/cochrane' # argv #'/mnt/c/Users/Mayur\ Sonawane/Downloads/cochrane/text' #'C:/Users/Mayur Sonawane/Downloads/cochrane/text'

    #split text file to single records
    txt_files = getTextFiles(path)
    print("text file",txt_files)
    for file in txt_files:
        splittxt(file)

    #coonvert single text records to jsons
    txt_files = getTextFiles(path + '/text')
    print('single record.txt',txt_files)
    for file in txt_files:
        # texttojson.splittxt(file)
        txtToJson(file)


    print('Done')



if __name__ == '__main__':
    main()#(sys.argv[1])



